/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#![warn(missing_docs)]

//! Parsing functionality for the GDScript language.

pub mod ast;
pub mod concrete;

/// Type used for indexing into the token-buffer.
pub type TokenIndex = usize;

#[cfg(test)]
mod tests {

    use super::concrete::*;
    use codespan::{FileMap, FileName};
    use gdscript_lexer::lexer::Tokenizer;
    use gdscript_lexer::token::Token;

    #[derive(Clone)]
    pub struct Structure(pub(crate) Kind, pub(crate) Vec<Structure>);

    pub fn s(kind: Kind, children: &[Structure]) -> Structure {
        Structure(kind, children.to_vec())
    }

    fn structure_assert(tree: Node, structure: Structure) {
        assert_eq!(tree.kind, structure.0);

        assert_eq!(tree.children.len(), structure.1.len());

        for (node, struc) in tree.children.into_iter().zip(structure.1) {
            structure_assert(node, struc);
        }
    }

    fn tokenize(src: &str) -> (FileMap<&str>, Vec<Token>) {
        let file_map = FileMap::new(FileName::virtual_("test"), src);

        let mut tokenizer = Tokenizer::new(&file_map);
        let toks = tokenizer.tokenize();

        (file_map, toks)
    }

    #[test]
    fn empty() {
        {
            let src = r#""#;

            let (file_map, toks) = tokenize(src);

            let tree = parse_file(&file_map, &toks);

            structure_assert(tree, s(Kind::File, &[]));
        }
    }

    #[test]
    fn file_comment() {
        {
            let src = r#"
# Hello world
        "#;

            let (file_map, toks) = tokenize(src);

            let tree = parse_file(&file_map, &toks);

            structure_assert(
                tree,
                s(
                    Kind::File,
                    &[
                        s(Kind::Newline, &[]),
                        s(Kind::Comment, &[s(Kind::Newline, &[])]),
                    ],
                ),
            );
        }

        {
            let src = r#"
# Hello world
"""
This is a doc comment
"""
        "#;

            let (file_map, toks) = tokenize(src);

            let tree = parse_file(&file_map, &toks);

            structure_assert(
                tree,
                s(
                    Kind::File,
                    &[
                        s(Kind::Newline, &[]),
                        s(Kind::Comment, &[s(Kind::Newline, &[])]),
                        s(
                            Kind::ClassItemDocComment,
                            &[s(Kind::DocComment, &[]), s(Kind::Newline, &[])],
                        ),
                    ],
                ),
            );
        }
    }

    #[test]
    fn file_items() {
        // tool
        {
            let src = r#"tool"#;

            let (file_map, toks) = tokenize(src);

            let tree = parse_file(&file_map, &toks);

            structure_assert(tree, s(Kind::File, &[s(Kind::Tool, &[s(Kind::End, &[])])]));
        }
        {
            let src = r#"tool
            "#;

            let (file_map, toks) = tokenize(src);

            let tree = parse_file(&file_map, &toks);

            structure_assert(
                tree,
                s(Kind::File, &[s(Kind::Tool, &[s(Kind::Newline, &[])])]),
            );
        }

        // extends
        {
            {
                let src = r#"extends"#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(Kind::Extends, &[s(Kind::Error, &[]), s(Kind::End, &[])])],
                    ),
                );
            }

            {
                let src = r#"extends A"#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::Extends,
                            &[
                                s(Kind::ExtendsPath, &[s(Kind::Identifier, &[])]),
                                s(Kind::End, &[]),
                            ],
                        )],
                    ),
                );
            }

            {
                let src = r#"extends "test".A.B"#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::Extends,
                            &[
                                s(
                                    Kind::ExtendsPath,
                                    &[
                                        s(
                                            Kind::ExtendsPath,
                                            &[
                                                s(Kind::ExtendsPath, &[s(Kind::String, &[])]),
                                                s(Kind::Period, &[]),
                                                s(Kind::Identifier, &[]),
                                            ],
                                        ),
                                        s(Kind::Period, &[]),
                                        s(Kind::Identifier, &[]),
                                    ],
                                ),
                                s(Kind::End, &[]),
                            ],
                        )],
                    ),
                );
            }
        }

        // class_name
        {
            {
                let src = r#"class_name"#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::ClassItemClassName,
                            &[s(Kind::Error, &[]), s(Kind::End, &[])],
                        )],
                    ),
                );
            }

            {
                let src = r#"class_name Test"#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::ClassItemClassName,
                            &[s(Kind::Identifier, &[]), s(Kind::End, &[])],
                        )],
                    ),
                );
            }
            {
                let src = r#"class_name Test # Test"#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::ClassItemClassName,
                            &[
                                s(Kind::Identifier, &[]),
                                s(Kind::Comment, &[s(Kind::End, &[])]),
                            ],
                        )],
                    ),
                );
            }
            {
                let src = r#"class_name Test "res://icon.png" "#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::ClassItemClassName,
                            &[
                                s(Kind::Identifier, &[]),
                                s(Kind::String, &[]),
                                s(Kind::End, &[]),
                            ],
                        )],
                    ),
                );
            }
            {
                // test newline-ignore
                let src = r#"class_name \
                Test "res://icon.png" "#;

                let (file_map, toks) = tokenize(src);

                let tree = parse_file(&file_map, &toks);

                structure_assert(
                    tree,
                    s(
                        Kind::File,
                        &[s(
                            Kind::ClassItemClassName,
                            &[
                                s(Kind::Identifier, &[]),
                                s(Kind::String, &[]),
                                s(Kind::End, &[]),
                            ],
                        )],
                    ),
                );
            }
        }
    }

}
