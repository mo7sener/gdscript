/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Parsing of tokens into a concrete syntax tree.

pub mod class;

use crate::TokenIndex;
use codespan::{ColumnIndex, FileMap, LineIndex};
use gdscript_lexer::token::{
    Location, Punctuation, Span, Token, TokenContent, TokenKind, TokenKind as TK,
};

/// The kind of a node describes what data it represents.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
#[allow(missing_docs)]
pub enum Kind {
    /// Unexpected input.
    Error,
    /// Lexical error encountered.
    LexicalError,

    End,

    Newline,
    Semicolon,
    Period,

    ClassItemDocComment,
    DocComment,

    ClassItemClassName,

    Comment,

    File,

    Tool,

    Extends,
    ExtendsPath,

    Identifier,

    String,
}

/// The "weakly typed" building-block of a concrete-syntax-tree.
#[derive(Debug, Clone)]
pub struct Node {
    /// The kind of node.
    pub kind: Kind,
    /// The span of the node an all it's child-nodes.
    pub span: (TokenIndex, TokenIndex),
    /// List of child nodes associated with this node.
    pub children: Vec<Node>,
}

impl Node {
    /// Check if the current node is an error.
    #[inline]
    pub fn is_error(&self) -> bool {
        self.kind == Kind::Error
    }

    /// Adds a node as a child node and adjusts the token-span.
    pub fn add_child(&mut self, node: Node) {
        self.span.1 = node.span.1;
        self.children.push(node);
    }
}

/// Shorthand for parsing a token-list into a GDScript-file.
///
/// See the [`File`] type.
///
/// [`File`]: ./struct.File.html
pub fn parse_file<Src: AsRef<str>>(file_map: &FileMap<Src>, tokens: &[Token]) -> Node {
    let mut parser = ParseContext::new(file_map, tokens);

    parse::<File, _>(&mut parser)
}

trait Parse {
    fn detect(tok: TokenContent) -> bool;

    fn parse(parser: &mut ParseContext<'_, impl AsRef<str>>) -> Node;
}

fn detect<T: Parse>(tok: TokenContent) -> bool {
    T::detect(tok)
}

fn parse<T: Parse, Src: AsRef<str>>(parser: &mut ParseContext<'_, Src>) -> Node {
    T::parse(parser)
}

pub(crate) struct ParseContext<'a, Src: AsRef<str>> {
    _file_map: &'a FileMap<Src>,
    toks: &'a [Token],

    tok_idx: TokenIndex,
}

impl<'a, Src: AsRef<str>> ParseContext<'a, Src> {
    fn new(file_map: &'a FileMap<Src>, tokens: &'a [Token]) -> ParseContext<'a, Src> {
        ParseContext {
            _file_map: file_map,
            toks: tokens,
            tok_idx: 0,
        }
    }

    fn _location(&self, idx: usize) -> (LineIndex, ColumnIndex) {
        let tok = self.toks[idx];

        let start = tok.span.start();

        self._file_map.location(start).unwrap()
    }

    fn is(&mut self, kind: TokenKind) -> bool {
        let tok = self.peek();

        tok.content.is_kind(kind)
    }

    fn peek(&mut self) -> Token {
        if let Some(tok) = self.toks.get(self.tok_idx) {
            *tok
        } else {
            Token {
                span: Span::new(Location::none(), Location::none()),
                content: TokenContent::End,
            }
        }
    }

    fn advance(&mut self) {
        // TODO do stuff like reset flags and all that?
        self.tok_idx += 1;
    }

    fn _lookahead(&self) -> Token {
        if let Some(tok) = self.toks.get(self.tok_idx + 1) {
            *tok
        } else {
            Token {
                span: Span::new(Location::none(), Location::none()),
                content: TokenContent::End,
            }
        }
    }

    fn emit_token(&mut self, kind: Kind) -> Node {
        let node = Node {
            kind,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        self.advance();

        node
    }
    fn emit_error(&mut self) -> Node {
        let node = Node {
            kind: Kind::Error,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        self.advance();

        node
    }

    fn expect(&mut self, tok_kind: TokenKind, kind: Kind) -> Node {
        let kind = if self.is(tok_kind) { kind } else { Kind::Error };

        let node = Node {
            kind,
            span: (self.tok_idx, self.tok_idx),
            children: vec![],
        };

        self.advance();

        node
    }
}

/// A GDScript file representing a class.
pub struct File;

impl Parse for File {
    fn detect(tok: TokenContent) -> bool {
        if tok == TokenContent::End {
            return true;
        }

        if detect::<LineTerminator>(tok) {
            return true;
        }

        if detect::<class::Item>(tok) {
            return true;
        }
        false
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        let mut children = vec![];

        loop {
            let tok = parser.peek();

            if tok.content.is_kind(TK::End) {
                break;
            }

            if detect::<class::Item>(tok.content) {
                let item = parse::<class::Item, _>(parser);
                children.push(item);
            } else if detect::<LineTerminator>(tok.content) {
                let newline = parse::<LineTerminator, _>(parser);
                children.push(newline);
            } else {
                match tok.content.get_kind() {
                    TokenKind::Error => {
                        children.push(parser.emit_token(Kind::LexicalError));
                        break;
                    }
                    TokenKind::End => {
                        break;
                    }
                    _ => {
                        // invalid token
                        children.push(parser.emit_error())
                    }
                }
            }
        }

        Node {
            kind: Kind::File,
            span: child_span(&children),
            children,
        }
    }
}

pub(crate) fn child_span(children: &[Node]) -> (TokenIndex, TokenIndex) {
    let first = if let Some(node) = children.first() {
        node.span.0
    } else {
        0
    };

    let last = if let Some(node) = children.last() {
        node.span.1
    } else {
        first
    };

    (first, last)
}

pub(crate) struct LineTerminator;

impl Parse for LineTerminator {
    fn detect(tok: TokenContent) -> bool {
        if tok.is_kind(TK::Newline) {
            true
        } else if tok.is_kind(TK::End) {
            true
        } else if tok.is_kind(TK::Comment) {
            true
        } else if tok == TokenContent::Punctuation(Punctuation::Semicolon) {
            true
        } else {
            false
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        let tok = parser.peek();

        if tok.content.is_kind(TK::Newline) {
            parser.emit_token(Kind::Newline)
        } else if tok.content.is_kind(TK::Comment) {
            let mut comment = parser.emit_token(Kind::Comment);

            // WARNING this recurses here, but since a comment is terminated
            // by a newline (or end of stream) it is guaranteed that this
            // recursion terminates.
            let actual_terminator = Self::parse(parser);

            comment.add_child(actual_terminator);

            comment
        } else if tok.content.is_kind(TK::End) {
            parser.emit_token(Kind::End)
        } else if tok.content == TokenContent::Punctuation(Punctuation::Semicolon) {
            parser.emit_token(Kind::Semicolon)
        } else {
            parser.emit_error()
        }
    }
}
