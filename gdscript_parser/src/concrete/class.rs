/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Types to parse class-related syntax.

use crate::concrete::{child_span, detect, parse, Kind, LineTerminator, Node, Parse, ParseContext};
use gdscript_lexer::token::{Keyword, Punctuation, TokenContent, TokenKind};

/// An "inner" class.
pub struct Class;

impl Parse for Class {
    fn detect(tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::Class | Keyword::Tool => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        unimplemented!()
    }
}

/// A class item.
///
/// All current class-items are:
///  - Member variables
///  - functions (member and static)
///  - inner types (such as enums and classes)
///  - "tool", for editor-runnable classes.
///  - "extends", to specify an object-inheritance relationship
///  - "class_name", to create a unique name identifying the script
///  - documentation comments in the form of strings.
pub struct Item;

impl Parse for Item {
    fn detect(tok: TokenContent) -> bool {
        if detect::<Member>(tok) {
            return true;
        }

        if detect::<Function>(tok) {
            return true;
        }

        if detect::<InnerType>(tok) {
            return true;
        }

        match tok {
            TokenContent::Keyword(Keyword::Tool) => true,
            TokenContent::Keyword(Keyword::Extends) => true,
            TokenContent::Keyword(Keyword::ClassName) => true,
            TokenContent::String(_) => true,
            _ => false,
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        let tok = parser.peek();

        if detect::<Member>(tok.content) {
            parse::<Member, _>(parser)
        } else if detect::<Function>(tok.content) {
            parse::<Function, _>(parser)
        } else if detect::<InnerType>(tok.content) {
            parse::<InnerType, _>(parser)
        } else {
            match tok.content {
                TokenContent::Keyword(Keyword::Tool) => {
                    let mut tool = parser.emit_token(Kind::Tool);
                    let terminator = parse::<LineTerminator, _>(parser);

                    tool.add_child(terminator);

                    tool
                }
                TokenContent::Keyword(Keyword::Extends) => {
                    let mut extends = parser.emit_token(Kind::Extends);

                    let path = parse::<ExtendsPath, _>(parser);

                    let newline = parse::<LineTerminator, _>(parser);

                    extends.add_child(path);
                    extends.add_child(newline);

                    extends
                }
                TokenContent::Keyword(Keyword::ClassName) => {
                    let mut kwd = parser.emit_token(Kind::ClassItemClassName);

                    let name = parser.expect(TokenKind::Identifier, Kind::Identifier);
                    kwd.add_child(name);

                    // icon path thingy
                    if parser.is(TokenKind::String) {
                        kwd.add_child(parser.emit_token(Kind::String));
                    }

                    let newline = parse::<LineTerminator, _>(parser);
                    kwd.add_child(newline);

                    kwd
                }
                TokenContent::String(_) => {
                    let string = parser.emit_token(Kind::DocComment);

                    let newline = parse::<LineTerminator, _>(parser);

                    let children = vec![string, newline];

                    Node {
                        kind: Kind::ClassItemDocComment,
                        span: child_span(&children),
                        children,
                    }
                }
                _ => parser.emit_error(),
            }
        }
    }
}

/// Class member-variable.
pub struct Member;

impl Parse for Member {
    fn detect(tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::OnReady | Keyword::Export | Keyword::Const | Keyword::Var => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        unimplemented!()
    }
}

/// A static- or member-function in a class.
pub struct Function;

impl Parse for Function {
    fn detect(tok: TokenContent) -> bool {
        if detect::<FunctionAttr>(tok) {
            return true;
        }

        match tok {
            TokenContent::Keyword(Keyword::Func) => true,
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        unimplemented!()
    }
}

/// Function attributes.
pub struct FunctionAttr;

impl Parse for FunctionAttr {
    fn detect(tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::Static
                | Keyword::Rpc
                | Keyword::Sync
                | Keyword::Master
                | Keyword::Puppet
                | Keyword::Slave
                | Keyword::RemoteSync
                | Keyword::MasterSync
                | Keyword::PuppetSync => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        unimplemented!()
    }
}

/// An inner type of a class, such as enums or other classes.
pub struct InnerType;

impl Parse for InnerType {
    fn detect(tok: TokenContent) -> bool {
        match tok {
            TokenContent::Keyword(kwd) => match kwd {
                Keyword::Class | Keyword::Enum => true,
                _ => false,
            },
            _ => false,
        }
    }

    fn parse(_parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        unimplemented!()
    }
}

/// The path describing the super-class.
struct ExtendsPath;

impl Parse for ExtendsPath {
    fn detect(tok: TokenContent) -> bool {
        if tok.is_kind(TokenKind::String) {
            true
        } else if tok.is_kind(TokenKind::Identifier) {
            true
        } else {
            false
        }
    }

    fn parse(parser: &mut ParseContext<impl AsRef<str>>) -> Node {
        let tok = parser.peek();

        let initial_path = match tok.content.get_kind() {
            TokenKind::Identifier => {
                let ident = parser.emit_token(Kind::Identifier);

                Node {
                    kind: Kind::ExtendsPath,
                    span: ident.span,
                    children: vec![ident],
                }
            }
            TokenKind::String => {
                let string = parser.emit_token(Kind::String);

                Node {
                    kind: Kind::ExtendsPath,
                    span: string.span,
                    children: vec![string],
                }
            }
            _ => {
                return parser.emit_error();
            }
        };

        let mut node = initial_path;

        loop {
            let tok = parser.peek();

            // not a dot? Done!
            if tok.content != TokenContent::Punctuation(Punctuation::Period) {
                return node;
            }

            let dot = parser.emit_token(Kind::Period);

            let next_atom = parser.expect(TokenKind::Identifier, Kind::Identifier);

            let new_node = Node {
                kind: Kind::ExtendsPath,
                span: (node.span.0, next_atom.span.1),
                children: vec![node, dot, next_atom],
            };

            node = new_node;
        }
    }
}
