/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use codespan::{FileMap, FileName};
use gdscript_parser::concrete::Node;
use std::path::Path;

pub fn file_iter(path: impl AsRef<Path>) -> impl Iterator<Item = FileMap<String>> {
    use std::fs;

    fs::read_dir(path).unwrap().filter_map(|entry| {
        let entry = if let Ok(e) = entry {
            e
        } else {
            return None;
        };

        if entry.path().is_dir() {
            return None;
        }

        let string = fs::read_to_string(&entry.path()).ok()?;

        let file_name = entry.file_name();
        let file_name = FileName::real(file_name);

        Some(FileMap::new(file_name, string))
    })
}

/// Check if a tree contains any errors.
pub fn has_errors(node: &Node) -> bool {
    if node.is_error() {
        return true;
    }

    for child in &node.children {
        if has_errors(child) {
            return true;
        }
    }

    false
}
