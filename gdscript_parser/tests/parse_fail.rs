/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate gdscript_lexer;
extern crate gdscript_parser;

mod common;

use gdscript_lexer::lexer::Tokenizer;
use gdscript_parser::concrete::parse_file;

#[test]
fn parse_fail() -> Result<(), Box<dyn std::error::Error>> {
    for file in common::file_iter("./tests/fail") {
        let mut tokenizer = Tokenizer::new(&file);
        let toks = &tokenizer.tokenize();

        let tree = parse_file(&file, toks);

        assert_eq!(common::has_errors(&tree), true, "File {:?}", file.name());
    }

    Ok(())
}
