# This is a test.
# The test also consists of line comments, apparently.
extends Node

export(NodePath) var thing := @"Main/Test"

func test(a: Test) -> Test:
    """
    Hello World!
    """
    # The above is a "python style" doc comment
    return a

func add(a: int, b: int) -> int:
    var long_line := a + b + \
        c + d \
        + e

    return a + b

func max(a: int, b: int) -> int:
    if a > b:
        return a
    else:
        return b

func inc(a: int) -> int:
    return a + 1

var mapping: Dictionary = {
    "hello": {},
    "world": [],
    test: "Thing",
    12: 24,
    "answer": int(0.0042e2),
}

