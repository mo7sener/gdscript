/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
extern crate gdscript_lexer;

use codespan::{FileMap, FileName};

use std::fs;

#[test]
fn run() -> Result<(), Box<std::error::Error>> {
    use gdscript_lexer::lexer::Tokenizer;
    use gdscript_lexer::token::*;

    for entry in fs::read_dir("./tests/files/fail").unwrap() {
        let entry = if let Ok(e) = entry {
            e
        } else {
            continue;
        };

        let string = fs::read_to_string(&entry.path())?;

        let file_name = entry.file_name();
        let file_name = FileName::real(file_name);

        let file_map = FileMap::new(file_name, string.as_str());

        let mut tokenizer = Tokenizer::new(&file_map);
        let toks = tokenizer.tokenize();

        assert_ne!(toks.last().map(|t| t.content), Some(TokenContent::End));
    }

    for entry in fs::read_dir("./tests/files/succeed").unwrap() {
        let entry = if let Ok(e) = entry {
            e
        } else {
            continue;
        };

        let string = fs::read_to_string(&entry.path())?;
        let file_name = entry.file_name();
        let file_name = FileName::real(file_name);

        let file_map = FileMap::new(file_name, &string);

        let mut tokenizer = Tokenizer::new(&file_map);

        let toks = &tokenizer.tokenize();

        assert_eq!(toks.last().map(|t| t.content), Some(TokenContent::End));
    }
    Ok(())
}
