/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use codespan::ByteIndex;
use codespan::ByteSpan;
use std::cmp::Ordering;

/// A span in the source code.
pub type Span = ByteSpan;
/// A location in the source code.
pub type Location = ByteIndex;

/// List of token kinds. Used to check for a type of a token without
/// having to construct a value.
#[allow(missing_docs)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TokenKind {
    Keyword,
    Identifier,
    Operator,
    Punctuation,
    NodePath,
    String,
    Comment,
    Number,
    Newline,
    Error,
    End,
}

/// A "processing unit" of the lexing phase.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Token {
    /// The location inside of the input
    pub span: Span,

    /// The content of the token
    pub content: TokenContent,
}

impl Ord for Token {
    fn cmp(&self, other: &Token) -> Ordering {
        if self.span.end() < other.span.start() {
            Ordering::Less
        } else if self.span.start() > other.span.end() {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

impl PartialOrd for Token {
    fn partial_cmp(&self, other: &Token) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

//impl PartialOrd for Person {
//    fn partial_cmp(&self, other: &Person) -> Option<Ordering> {
//        Some(self.cmp(other))
//    }
//}
//
//impl PartialEq for Person {
//    fn eq(&self, other: &Person) -> bool {
//        self.height == other.height
//    }
//}

/// Denotes different types of tokens
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum TokenContent {
    /// A reserved GDScript keyword
    Keyword(Keyword),

    /// An identifier (such as a name)
    Identifier(Span),

    /// An operator (such as `+` or `==`)
    Operator(Operator),

    /// Punctuation (such as braces, comma, ...)
    Punctuation(Punctuation),

    /// A NodePath literal
    NodePath(StringLiteral),

    /// A string literal
    String(StringLiteral),

    /// A line comment
    Comment(Span),

    /// A number literal
    Number(Number),

    /// A newline character.
    Newline,

    /// Error token which is emitted when an unexpected input was encountered
    Error(Error),

    /// The end of the input was reached
    End,
}

impl TokenContent {
    /// Compute the kind of a given TokenContent.
    pub fn get_kind(self) -> TokenKind {
        use self::TokenKind as TK;

        match self {
            TokenContent::Keyword(_) => TK::Keyword,
            TokenContent::Identifier(_) => TK::Identifier,
            TokenContent::Operator(_) => TK::Operator,
            TokenContent::Punctuation(_) => TK::Punctuation,
            TokenContent::NodePath(_) => TK::NodePath,
            TokenContent::String(_) => TK::String,
            TokenContent::Comment(_) => TK::Comment,
            TokenContent::Number(_) => TK::Number,
            TokenContent::Error(_) => TK::Error,
            TokenContent::Newline => TK::Newline,
            TokenContent::End => TK::End,
        }
    }

    /// Check if the TokenKind of a given TokenContent matches.
    pub fn is_kind(self, kind: TokenKind) -> bool {
        self.get_kind() == kind
    }
}

/// Sign of a floating-point exponent
#[allow(missing_docs)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Sign {
    Positive,
    Negative,
}

/// Data describing a number literal
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Number {
    /// A number that uses hexadecimal encoding
    Hexadecimal {
        /// slice to data inside the source code
        content: Span,
    },
    /// A number that uses decimal encoding
    Decimal {
        /// slice to integer (natural) data inside the source code
        ///
        /// Note: can be an empty slice
        integer: Span,
        /// slice to fraction data inside the source code
        ///
        /// Note: can be an empty slice
        fraction: Span,
        /// exponent data (sign and slice inside source)
        ///
        /// Note: can be an empty slice
        exponent: (Sign, Span),
    },
}

#[allow(missing_docs)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Error {
    UnclosedStringLiteral,
    UnexpectedCharacter { expected: &'static str, got: char },
    UnexpectedEnd,
}

/// Data describing a string literal
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct StringLiteral {
    /// Type of string literal
    pub ty: StringType,
    /// The content as it appears inside of the source code. (unprocessed)
    ///
    /// TODO: create an iterator that yields `char`s from this
    pub content: Span,
}

/// Different kinds of string literals
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum StringType {
    /// Example `'hello'`
    SingleQuote,

    /// Example `"Hello"`
    DoubleQuote,

    /// Example `"""This can "contain" other quotes"""`
    MultiLine,
}

/// Enumeration of GDScript-keywords
#[allow(missing_docs)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Keyword {
    Selff,
    If,
    ElIf,
    Else,
    For,
    Do,
    While,
    Switch,
    Case,
    Break,
    Continue,
    Pass,
    Return,
    Match,
    Func,
    Class,
    ClassName,
    Extends,
    Is,
    OnReady,
    Tool,
    Static,
    Export,
    SetGet,
    Const,
    Var,
    As,
    Void,
    Enum,
    Preload,
    Assert,
    Yield,
    Signal,
    Breakpoint,
    Rpc,
    Sync,
    Master,
    Puppet,
    Slave,
    RemoteSync,
    MasterSync,
    PuppetSync,
}

/// Enumeration of GDScript-operators
#[allow(missing_docs)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Operator {
    In,
    Equal,
    NotEqual,
    Less,
    LessEqual,
    Greater,
    GreaterEqual,
    And,
    Or,
    Not,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    ShiftLeft,
    ShiftRight,
    Assign,
    AssignAdd,
    AssignSub,
    AssignMul,
    AssignDiv,
    AssignMod,
    AssignShiftLeft,
    AssignShiftRight,
    AssignBitAnd,
    AssignBitOr,
    AssignBitXor,
    BitAnd,
    BitOr,
    BitXor,
    BitInvert,
}

/// Enumeration of valid punctuation
#[allow(missing_docs)]
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum Punctuation {
    BracketOpen,
    BracketClose,

    CurlyBracketOpen,
    CurlyBracketClose,

    ParenthesisOpen,
    ParenthesisClose,

    Comma,
    Semicolon,
    Period,
    QuestionMark,
    Colon,
    Dollar,

    Arrow,
}
