/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use crate::token::{
    Error, Keyword, Number, Operator, Punctuation, Sign, StringType, Token, TokenContent,
};

use crate::token::Location;
use crate::token::Span;
use crate::token::StringLiteral;
use codespan::FileMap;
use codespan::{ByteIndex, ByteOffset, ByteSpan, ColumnIndex, FileName, LineIndex};
use std::intrinsics::transmute;
use std::iter::Peekable;
use std::str::CharIndices;

// ----- Tokenizer implementation -----

const OPERATOR_SYMBOLS: &[char] = &[
    '=', '!', '<', '>', '+', '-', '*', '/', '%', '&', '|', '^', '~',
];

const PUNCTUATION_SYMBOLS: &[char] = &['[', ']', '{', '}', '(', ')', ',', ';', '?', ':', '$'];

const PUNCTUATIONS: &[(char, Punctuation)] = &[
    ('[', Punctuation::BracketOpen),
    (']', Punctuation::BracketClose),
    ('{', Punctuation::CurlyBracketOpen),
    ('}', Punctuation::CurlyBracketClose),
    ('(', Punctuation::ParenthesisOpen),
    (')', Punctuation::ParenthesisClose),
    (',', Punctuation::Comma),
    (';', Punctuation::Semicolon),
    ('?', Punctuation::QuestionMark),
    (':', Punctuation::Colon),
    ('$', Punctuation::Dollar),
];

pub struct Tokenizer<'a, FileMapType: AsRef<str>> {
    reached_end: bool,

    // used for reading slices for identifiers
    pub file_map: &'a codespan::FileMap<FileMapType>,

    // peeking for single-character lookahead, indices for sub-slicing the utf8 data
    source: Peekable<CharIndices<'a>>,

    // For when the starting index is not Zero
    start_index: ByteOffset,

    pub report: Report,
}

pub struct Report {
    pub is_inc: bool,
    pub old_tok_count: usize,
    pub new_tok_count: usize,
}

impl Report {
    pub fn new() -> Report {
        Report {
            is_inc: false,
            old_tok_count: 0,
            new_tok_count: 0,
        }
    }
}

impl<'source, FileMapType: AsRef<str>> Tokenizer<'source, FileMapType> {
    fn peek(&mut self) -> Option<(Location, char)> {
        let start = self.file_map.span().start();
        self.source.peek().map(|(pos, c)| {
            let pos = start + ByteOffset::from(*pos as i64);
            // let pos = Location::from(*pos as u32);
            (pos, *c)
        })
    }
    /// Most basic use
    pub fn tokenize(&mut self) -> Vec<Token> {
        let new_toks: Vec<Token> = self.collect();
        self.report = Report {
            is_inc: false,
            old_tok_count: 0,
            new_tok_count: new_toks.len(),
        };
        new_toks
    }
    /// Skips unedited parts of the code
    pub fn tokenize_inc(
        &mut self,
        old_toks: Option<Vec<Token>>,
        edit_pos: Option<(u32, u32)>,
    ) -> Vec<Token> {
        if edit_pos.is_none() || old_toks.is_none() {
            return self.tokenize();
        }

        let edit_pos = edit_pos.unwrap();
        let old_toks = old_toks.unwrap();

        let byte_index = match self
            .file_map
            .byte_index(LineIndex(edit_pos.0), ColumnIndex(edit_pos.1))
            {
            Ok(i) => i,
            Err(_) => return self.tokenize(),
        };

        let search_tok = Token {
            span: ByteSpan::new(byte_index, byte_index),
            content: TokenContent::End,
        };

        let safe_tok = match old_toks.binary_search(&search_tok) {
            Ok(i) | Err(i) => {
                if i == 0 {
                    i
                } else {
                    i - 1
                }
            } // Index of last unedited token
        };

        let start_index = old_toks[safe_tok].span.end().to_usize();
        let new_code = &self.file_map.src()[(start_index - 1)..];

        let new_filemap = FileMap::new(FileName::virtual_("test"), new_code);
        let mut new_tokenizer = Tokenizer::new(&new_filemap);
        new_tokenizer.start_index = ByteOffset((start_index - 1) as i64);

        let mut new_toks = new_tokenizer.tokenize();

        let mut old_toks = old_toks[..(safe_tok + 1)].to_vec();

        let old_count = old_toks.len();
        let new_count = new_toks.len();

        self.report = Report {
            is_inc: true,
            old_tok_count: old_count,
            new_tok_count: new_count,
        };

        old_toks.append(&mut new_toks);

        old_toks
    }

    pub fn new(file_map: &'source FileMap<FileMapType>) -> Tokenizer<FileMapType> {
        Tokenizer {
            reached_end: false,
            file_map,
            source: file_map.src().char_indices().peekable(),
            start_index: ByteOffset(0),
            report: Report::new(),
        }
    }

    fn next(&mut self) -> Option<(Location, char)> {
        let start = self.file_map.span().start();
        self.source.next().map(|(pos, c)| {
            // let pos = Location::from(pos as u32);
            let pos = start + ByteOffset::from(pos as i64);
            (pos, c)
        })
    }

    fn lookahead_byte_or(&self, loc: Location, default: u8) -> u8 {
        self.lookahead_byte(loc).unwrap_or(default)
    }

    fn lookahead_byte(&self, loc: Location) -> Option<u8> {
        let start = self.file_map.span().start();

        let raw_index = (loc - start).to_usize();

        let src = self.file_map.src();

        let byte_src = src.as_bytes();

        if raw_index < byte_src.len() {
            Some(byte_src[raw_index])
        } else {
            None
        }
    }

    fn new_span(&self, start: ByteIndex, end: ByteIndex) -> Span {
        Span::new(start + self.start_index, end + self.start_index)
    }

    fn offset_remove(&self, span: Span) -> Span {
        Span::new(
            span.start() - self.start_index,
            span.end() - self.start_index,
        )
    }

    fn slice(&self, span: Span) -> &'source str {
        let slice = self.file_map.src_slice(self.offset_remove(span)).unwrap();
        unsafe { transmute(slice) }
    }

    fn read_identifier_or_keyword(&mut self, start: Location) -> Token {
        let mut end_pos = start;

        while let Some((next, c)) = self.peek() {
            if c.is_alphanumeric() || c == '_' {
                end_pos = next + ByteOffset::from_char_utf8(c);
                self.source.next();
            } else {
                break;
            }
        }

        let span = self.new_span(start, end_pos);

        let ident = self.slice(span);

        let content = {
            if let Some(keyword) = ident_to_keyword(ident) {
                TokenContent::Keyword(keyword)
            } else if let Some(op) = ident_to_operator(ident) {
                TokenContent::Operator(op)
            } else {
                TokenContent::Identifier(span)
            }
        };

        Token { span, content }
    }

    fn read_operator(&mut self, start: Location) -> Token {
        let (mut end_pos, c) = self
            .next()
            .expect("called read_operator(), reading first char should be safe");

        let content = match c {
            '=' => {
                if let Some((next, '=')) = self.peek() {
                    self.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::Equal)
                } else {
                    TokenContent::Operator(Operator::Assign)
                }
            }
            '!' => {
                if let Some((next, '=')) = self.peek() {
                    self.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::NotEqual)
                } else {
                    TokenContent::Operator(Operator::Not)
                }
            }
            '<' => {
                if let Some((next, '=')) = self.peek() {
                    self.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::LessEqual)
                } else if let Some((next, '<')) = self.peek() {
                    self.next();
                    end_pos = next + ByteOffset::from_char_utf8('<');

                    if let Some((next, '=')) = self.peek() {
                        self.next();
                        end_pos = next + ByteOffset::from_char_utf8('=');

                        TokenContent::Operator(Operator::AssignShiftLeft)
                    } else {
                        TokenContent::Operator(Operator::ShiftLeft)
                    }
                } else {
                    TokenContent::Operator(Operator::Less)
                }
            }
            '>' => {
                if let Some((next, '=')) = self.peek() {
                    self.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::GreaterEqual)
                } else if let Some((next, '>')) = self.peek() {
                    self.next();
                    end_pos = next + ByteOffset::from_char_utf8('>');

                    if let Some((next, '=')) = self.peek() {
                        self.source.next();
                        end_pos = next + ByteOffset::from_char_utf8('=');

                        TokenContent::Operator(Operator::AssignShiftRight)
                    } else {
                        TokenContent::Operator(Operator::ShiftRight)
                    }
                } else {
                    TokenContent::Operator(Operator::Greater)
                }
            }
            '+' => {
                if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignAdd)
                } else {
                    TokenContent::Operator(Operator::Add)
                }
            }
            '-' => {
                if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignSub)
                } else if let Some((next, '>')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('>');

                    TokenContent::Punctuation(Punctuation::Arrow)
                } else {
                    TokenContent::Operator(Operator::Sub)
                }
            }
            '*' => {
                if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignMul)
                } else {
                    TokenContent::Operator(Operator::Mul)
                }
            }
            '/' => {
                if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignDiv)
                } else {
                    TokenContent::Operator(Operator::Div)
                }
            }
            '%' => {
                if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignMod)
                } else {
                    TokenContent::Operator(Operator::Mod)
                }
            }
            '&' => {
                if let Some((next, '&')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('&');

                    TokenContent::Operator(Operator::And)
                } else if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignBitAnd)
                } else {
                    TokenContent::Operator(Operator::BitAnd)
                }
            }
            '|' => {
                if let Some((next, '|')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('|');

                    TokenContent::Operator(Operator::Or)
                } else if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignBitOr)
                } else {
                    TokenContent::Operator(Operator::BitOr)
                }
            }
            '^' => {
                if let Some((next, '=')) = self.peek() {
                    self.source.next();
                    end_pos = next + ByteOffset::from_char_utf8('=');

                    TokenContent::Operator(Operator::AssignBitXor)
                } else {
                    TokenContent::Operator(Operator::BitXor)
                }
            }
            '~' => TokenContent::Operator(Operator::BitInvert),
            c => unreachable!("unexpected symbol {:?} as operator", c),
        };

        let span = self.new_span(start, end_pos);

        Token { span, content }
    }

    fn read_string(&mut self, start: Location) -> Token {
        let (mut end_pos, c) = self
            .next()
            .expect("called read_string, first character should be known and exist");

        let mut ty = StringType::SingleQuote;

        if c == '\'' {
            ty = StringType::SingleQuote;
        } else if c == '"' {
            ty = StringType::DoubleQuote;

            if let Some((next, '"')) = self.peek() {
                // two double quotes, this could either be an empty string or the beginning
                // of a multi-line string
                // Since the " character is ascii and is contained in a single byte, checking if
                // the next byte in the string is equal to it is sufficient to have a lookahead

                if let Some(b'"') = self.lookahead_byte(next + ByteOffset::from(1)) {
                    ty = StringType::MultiLine;

                    self.next();
                    self.next();
                }
            }
        }

        // get the starting position
        let (content_start, _) = match self.peek() {
            Some(val) => val,
            None => {
                self.reached_end = true;

                let span = self.new_span(start, end_pos);

                return Token {
                    span,
                    content: TokenContent::Error(Error::UnclosedStringLiteral),
                };
            }
        };

        let content_end = loop {
            let (pos, c) = match self.peek() {
                Some(val) => val,
                None => {
                    self.reached_end = true;
                    let span = self.new_span(start, end_pos);
                    return Token {
                        span,
                        content: TokenContent::Error(Error::UnclosedStringLiteral),
                    };
                }
            };

            // check terminators
            match c {
                '\'' if ty == StringType::SingleQuote => {
                    self.next();
                    end_pos = pos + ByteOffset::from_char_utf8('\'');
                    break pos;
                }

                '"' if ty == StringType::DoubleQuote => {
                    self.next();
                    end_pos = pos + ByteOffset::from_char_utf8('"');
                    break pos;
                }

                '"' if ty == StringType::MultiLine => {
                    let lookahead_chars = (
                        self.lookahead_byte(pos + ByteOffset::from(1)),
                        self.lookahead_byte(pos + ByteOffset::from(2)),
                    );

                    match lookahead_chars {
                        // string terminated?
                        (Some(b'"'), Some(b'"')) => {
                            self.next();
                            self.next();
                            self.next();

                            let size = ByteOffset::from_char_utf8('"');
                            end_pos = pos + size + size + size;

                            break pos;
                        }
                        // reached end of input?
                        (None, _) | (_, None) => {
                            self.reached_end = true;
                            let end = self.file_map.span().end();
                            let span = self.new_span(start, end);
                            return Token {
                                span,
                                content: TokenContent::Error(Error::UnclosedStringLiteral),
                            };
                        }
                        // not a terminator and not the end of stream, just continue
                        _ => {}
                    }
                }

                _ => {}
            }

            match c {
                '\\' => {
                    self.source.next();
                    end_pos = pos + ByteOffset::from_char_utf8('\\');

                    if let Some((next, c)) = self.peek() {
                        match c {
                            'a' | 'b' | 't' | 'n' | 'v' | 'f' | 'r' | '\'' | '\"' | '\\' | '/'
                            | 'u' => {
                                self.source.next();
                                end_pos = next + ByteOffset::from_char_utf8(c);
                            }
                            _ => {}
                        }
                    } else {
                        self.reached_end = true;
                        let span = self.new_span(start, end_pos);
                        return Token {
                            span,
                            content: TokenContent::Error(Error::UnclosedStringLiteral),
                        };
                    }
                }

                c => {
                    self.next();
                    end_pos = pos + ByteOffset::from_char_utf8(c);
                }
            }
        };

        let span = self.new_span(start, end_pos);

        let string_span = self.new_span(content_start, content_end);

        Token {
            span,
            content: TokenContent::String(StringLiteral {
                ty,
                content: string_span,
            }),
        }
    }

    fn read_number(&mut self, start: Location) -> Token {
        #[derive(Copy, Clone, Debug, Eq, PartialEq)]
        enum State {
            InitialDigits,
            Fraction,
            Exponent,
        }

        #[derive(Copy, Clone, Debug, Eq, PartialEq)]
        enum NumType {
            Hex,
            Dec,
        }

        let (digit_start, c) = self.peek().unwrap();

        let mut end_pos = digit_start;

        // parse "dot"
        if c == '.' {
            // no number - dot!
            if !(self.lookahead_byte_or(digit_start + ByteOffset::from(1), 0) as char).is_digit(10)
            {
                self.next();
                end_pos = digit_start + ByteOffset::from_char_utf8(c);

                let span = self.new_span(start, end_pos);

                return Token {
                    span,
                    content: TokenContent::Punctuation(Punctuation::Period),
                };
            }
        }

        let ty = if c == '0' {
            if self.lookahead_byte_or(digit_start + ByteOffset::from(1), 0) == b'x' {
                self.source.next();
                self.source.next();

                end_pos =
                    digit_start + ByteOffset::from_char_utf8('0') + ByteOffset::from_char_utf8('x');

                NumType::Hex
            } else {
                NumType::Dec
            }
        } else {
            NumType::Dec
        };

        let mut state = State::InitialDigits;

        let (digit_start, mut digit_end) = {
            let pos = self.peek().unwrap().0;
            (pos, pos)
        };

        let (mut frac_start, mut frac_end) = (digit_start, digit_start);

        let mut exp_beginning = true;
        let mut sign = Sign::Positive;

        let (mut exp_start, mut exp_end) = (digit_start, digit_start);

        while let Some((pos, c)) = self.peek() {
            match ty {
                NumType::Dec => match state {
                    State::InitialDigits => {
                        if c == '.' {
                            self.next();

                            end_pos = pos + ByteOffset::from_char_utf8('.');
                            frac_start = end_pos;
                            digit_end = pos;

                            state = State::Fraction;
                            continue;
                        }
                        if c == 'e' {
                            self.next();

                            digit_end = pos;
                            end_pos = pos + ByteOffset::from_char_utf8('e');
                            exp_start = end_pos;

                            state = State::Exponent;
                            continue;
                        }

                        if c.is_digit(10) {
                            self.source.next();
                            end_pos = pos + ByteOffset::from_char_utf8(c);
                            digit_end = end_pos;
                        } else {
                            digit_end = pos;

                            let span = self.new_span(start, end_pos);

                            return Token {
                                span,
                                content: TokenContent::Number(Number::Decimal {
                                    integer: self.new_span(digit_start, digit_end),
                                    fraction: self.new_span(frac_start, frac_end),
                                    exponent: (sign, self.new_span(exp_start, exp_end)),
                                }),
                            };
                        }
                    }
                    State::Fraction => {
                        if c == 'e' {
                            self.source.next();

                            end_pos = pos + ByteOffset::from_char_utf8('e');
                            frac_end = pos;
                            exp_start = end_pos;

                            state = State::Exponent;
                            continue;
                        }

                        if c.is_digit(10) {
                            self.source.next();
                            end_pos = pos + ByteOffset::from_char_utf8(c);
                            frac_end = pos;
                        } else {
                            frac_end = pos;
                            let span = self.new_span(start, end_pos);

                            return Token {
                                span,
                                content: TokenContent::Number(Number::Decimal {
                                    integer: self.new_span(digit_start, digit_end),
                                    fraction: self.new_span(frac_start, frac_end),
                                    exponent: (sign, self.new_span(exp_start, exp_end)),
                                }),
                            };
                        }
                    }
                    State::Exponent => {
                        if exp_beginning && (c == '+' || c == '-') {
                            sign = if c == '+' {
                                Sign::Positive
                            } else {
                                Sign::Negative
                            };
                            self.source.next();

                            end_pos = pos + ByteOffset::from_char_utf8(c);

                            exp_start = end_pos;
                            exp_end = exp_start;

                            exp_beginning = false;
                            continue;
                        } else if exp_beginning {
                            exp_beginning = false;
                        }

                        if c.is_digit(10) {
                            self.source.next();
                            end_pos = pos + ByteOffset::from_char_utf8(c);
                            exp_end = pos;
                        } else {
                            exp_end = pos;
                            let span = self.new_span(start, end_pos);

                            return Token {
                                span,
                                content: TokenContent::Number(Number::Decimal {
                                    integer: self.new_span(digit_start, digit_end),
                                    fraction: self.new_span(frac_start, frac_end),
                                    exponent: (sign, self.new_span(exp_start, exp_end)),
                                }),
                            };
                        }
                    }
                },
                NumType::Hex => {
                    if c.is_ascii_hexdigit() || c == '_' {
                        self.source.next();
                        end_pos = pos + ByteOffset::from_char_utf8(c);

                        digit_end = end_pos;
                    } else {
                        return Token {
                            span: self.new_span(start, end_pos),
                            content: TokenContent::Number(Number::Hexadecimal {
                                content: self.new_span(digit_start, digit_end),
                            }),
                        };
                    }
                }
            }
        }

        // after the loop means end of source but no number terminator was read
        let span = self.new_span(start, end_pos);
        match ty {
            NumType::Hex => Token {
                span,
                content: TokenContent::Number(Number::Hexadecimal {
                    content: self.new_span(digit_start, end_pos),
                }),
            },
            NumType::Dec => match state {
                State::InitialDigits => Token {
                    span,
                    content: TokenContent::Number(Number::Decimal {
                        integer: self.new_span(digit_start, end_pos),
                        fraction: self.new_span(frac_start, frac_end),
                        exponent: (sign, self.new_span(exp_start, exp_end)),
                    }),
                },
                State::Fraction => Token {
                    span,
                    content: TokenContent::Number(Number::Decimal {
                        integer: self.new_span(digit_start, digit_end),
                        fraction: self.new_span(frac_start, end_pos),
                        exponent: (sign, self.new_span(exp_start, exp_end)),
                    }),
                },
                State::Exponent => Token {
                    span,
                    content: TokenContent::Number(Number::Decimal {
                        integer: self.new_span(digit_start, digit_end),
                        fraction: self.new_span(frac_start, frac_end),
                        exponent: (sign, self.new_span(exp_start, end_pos)),
                    }),
                },
            },
        }
    }
}

impl<'source, FileMapType: AsRef<str>> Iterator for Tokenizer<'source, FileMapType> {
    type Item = Token;

    // When this function is called the tokenizer "head" is right at the start of a token.
    // This means that after a token is consumed, any trailing whitespace has to be consumed as well
    // (except after line breaks)
    fn next(&mut self) -> Option<Token> {
        if self.reached_end {
            return None;
        }

        // consume any leading whitespace
        let (peek_pos, peek_c) = loop {
            if let Some((pos, c)) = self.peek() {
                if c == '\\' {
                    let next_char =
                        self.lookahead_byte_or(pos + ByteOffset::from_char_utf8('\\'), 0);

                    let valid_nix_newline = next_char == b'\n';
                    let valid_win_newline = next_char == b'\r'
                        && self.lookahead_byte_or(pos + ByteOffset::from_str("\\\r"), 0) == b'\n';

                    let valid_newline = valid_nix_newline || valid_win_newline;

                    if !valid_newline {
                        self.source.next();
                        self.reached_end = true;

                        let end = pos + ByteOffset::from_char_utf8(c);

                        let span = self.new_span(pos, end);

                        return Some(Token {
                            span,
                            content: TokenContent::Error(Error::UnexpectedCharacter {
                                got: self.peek().map(|(_, c)| c).unwrap_or(0 as char),
                                expected: "newline after '\\'",
                            }),
                        });
                    }
                    if next_char == b'\r' {
                        self.source.next();
                    }
                    self.source.next();
                    self.source.next();
                    continue;
                }

                if !c.is_whitespace() || c == '\n' {
                    break (pos, c);
                } else {
                    self.source.next();
                }
            } else {
                // uuuuuuh no data read? Well that means the end was reached.
                self.reached_end = true;

                let pos = self.file_map.span().end();

                let span = self.new_span(pos, pos);

                return Some(Token {
                    span,
                    content: TokenContent::End,
                });
            }
        };

        let token = match peek_c {
            // identifier or keyword
            c if c == '_' || c.is_alphabetic() => self.read_identifier_or_keyword(peek_pos),

            c if OPERATOR_SYMBOLS.contains(&c) => self.read_operator(peek_pos),

            c if PUNCTUATION_SYMBOLS.contains(&c) => {
                let start = peek_pos;
                let end_pos = start + ByteOffset::from_char_utf8(c);

                self.source.next();

                let span = self.new_span(start, end_pos);

                let (_, punct) = PUNCTUATIONS.iter().find(|(ch, _)| *ch == c).expect(
                    "character matched punctuation symbols, \
                     entry for punctuation type should be present",
                );

                Token {
                    span,
                    content: TokenContent::Punctuation(*punct),
                }
            }

            '\'' | '"' => self.read_string(peek_pos),

            '@' => {
                let start = peek_pos;

                self.source.next();

                if let Some((pos, c)) = self.peek() {
                    if c == '\'' || c == '\"' {
                        // gud
                        let mut str_tok = self.read_string(pos);
                        str_tok.span = self.new_span(start, str_tok.span.end());

                        str_tok.content = match str_tok.content {
                            TokenContent::String(sl) => TokenContent::NodePath(sl),
                            other => other,
                        };

                        str_tok
                    } else {
                        self.reached_end = true;
                        Token {
                            span: self.new_span(start, start),
                            content: TokenContent::Error(Error::UnexpectedCharacter {
                                got: c,
                                expected: "beginning quote of a string constant",
                            }),
                        }
                    }
                } else {
                    self.reached_end = true;
                    Token {
                        span: self.new_span(start, start),
                        content: TokenContent::Error(Error::UnexpectedEnd),
                    }
                }
            }

            // comment
            '#' => {
                let start = peek_pos;

                self.source.next();

                let comment_start = start + ByteOffset::from_char_utf8('#');

                let end_pos;
                loop {
                    let (pos, c) = if let Some(val) = self.peek() {
                        val
                    } else {
                        end_pos = self.file_map.span().end();
                        break;
                    };

                    match c {
                        '\n' => {
                            end_pos = pos;
                            break;
                        }
                        _ => {
                            self.source.next();
                        }
                    }
                }

                let span = self.new_span(start, end_pos);

                let comment_span = self.new_span(comment_start, end_pos);

                Token {
                    span,
                    content: TokenContent::Comment(comment_span),
                }
            }

            c if c.is_digit(10) || c == '.' => self.read_number(peek_pos),

            '\n' => {
                let start = peek_pos;
                let end = peek_pos + ByteOffset::from_char_utf8('\n');

                self.source.next();

                let span = self.new_span(start, end);

                Token {
                    span,
                    content: TokenContent::Newline,
                }
            }

            c => {
                let start = peek_pos;
                let end_pos = start + ByteOffset::from_char_utf8(c);

                self.source.next();

                let span = self.new_span(start, end_pos);

                Token {
                    span,
                    content: TokenContent::Error(Error::UnexpectedCharacter {
                        got: c,
                        expected: "next token",
                    }),
                }
            }
        };

        Some(token)
    }
}

fn ident_to_keyword(s: &str) -> Option<Keyword> {
    use crate::token::Keyword::*;

    match s {
        "self" => Some(Selff),
        "if" => Some(If),
        "elif" => Some(ElIf),
        "else" => Some(Else),
        "for" => Some(For),
        "do" => Some(Do),
        "while" => Some(While),
        "switch" => Some(Switch),
        "case" => Some(Case),
        "break" => Some(Break),
        "continue" => Some(Continue),
        "pass" => Some(Pass),
        "return" => Some(Return),
        "match" => Some(Match),
        "func" => Some(Func),
        "class" => Some(Class),
        "class_name" => Some(ClassName),
        "extends" => Some(Extends),
        "is" => Some(Is),
        "onready" => Some(OnReady),
        "tool" => Some(Tool),
        "static" => Some(Static),
        "export" => Some(Export),
        "setget" => Some(SetGet),
        "const" => Some(Const),
        "var" => Some(Var),
        "as" => Some(As),
        "void" => Some(Void),
        "enum" => Some(Enum),
        "preload" => Some(Preload),
        "assert" => Some(Assert),
        "yield" => Some(Yield),
        "signal" => Some(Signal),
        "breakpoint" => Some(Breakpoint),
        "rpc" => Some(Rpc),
        "sync" => Some(Sync),
        "master" => Some(Master),
        "puppet" => Some(Puppet),
        "slave" => Some(Slave),
        "remotesync" => Some(RemoteSync),
        "mastersync" => Some(MasterSync),
        "puppetsync" => Some(PuppetSync),
        _ => None,
    }
}

fn ident_to_operator(s: &str) -> Option<Operator> {
    match s {
        "in" => Some(Operator::In),
        "and" => Some(Operator::And),
        "or" => Some(Operator::Or),
        "not" => Some(Operator::Not),
        _ => None,
    }
}

// ----- Public interface -----
