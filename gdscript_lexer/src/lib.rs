/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![warn(missing_docs)]

//! Lexing library for the GDScript programming language.

/// Definition of the "atoms" of this lexer: tokens
pub mod token;

/// Implementation of the tokenizer
pub mod lexer;

#[cfg(test)]
mod tests {
    use super::token::TokenContent::*;
    use crate::lexer::Report;
    use crate::lexer::Tokenizer;
    use crate::token::Token;
    use crate::token::{Sign, StringType, TokenContent};
    use codespan::{FileMap, FileName};

    struct TokTest<'a> {
        file_map: FileMap<&'a str>,
        toks: Vec<Token>,
        report: Report,
    }

    impl<'source> TokTest<'source> {
        pub fn new(src: &'static str) -> Self {
            TokTest {
                file_map: FileMap::new(FileName::virtual_("test"), src),
                toks: Vec::new(),
                report: Report::new(),
            }
        }

        pub fn tokenize(&mut self) {
            let mut tokenizer = Tokenizer::new(&self.file_map);
            self.toks = tokenizer.tokenize();
            self.report = tokenizer.report;
        }

        pub fn tokenize_inc(&mut self, old_toks: Option<Vec<Token>>, edit_pos: Option<(u32, u32)>) {
            let mut tokenizer = Tokenizer::new(&self.file_map);
            self.toks = tokenizer.tokenize_inc(old_toks, edit_pos);
            self.report = tokenizer.report;
        }

        pub fn identifier(&self, token: Token) -> &str {
            if let TokenContent::Identifier(span) = token.content {
                let content = self.file_map.src_slice(span).unwrap();

                content
            } else {
                "error"
            }
        }

        pub fn number(&self, token: Token) -> (&str, &str, Sign, &str) {
            if let TokenContent::Number(num) = token.content {
                use crate::token::Number as N;

                if let N::Decimal {
                    integer,
                    fraction,
                    exponent,
                } = num
                {
                    let integer_slice = self.file_map.src_slice(integer).unwrap();
                    let fraction_slice = self.file_map.src_slice(fraction).unwrap();
                    let exponent_slice = self.file_map.src_slice(exponent.1).unwrap();

                    (integer_slice, fraction_slice, exponent.0, exponent_slice)
                } else {
                    ("error", "error", Sign::Negative, "error")
                }
            } else {
                ("error", "error", Sign::Negative, "error")
            }
        }

        pub fn num_hex(&self, token: Token) -> &str {
            if let TokenContent::Number(num) = token.content {
                use crate::token::Number as N;

                if let N::Hexadecimal {
                    content: content_span,
                } = num
                {
                    let slice = self.file_map.src_slice(content_span).unwrap();

                    slice
                } else {
                    "error"
                }
            } else {
                "error"
            }
        }

        pub fn string(&self, token: Token) -> (StringType, &str) {
            if let TokenContent::String(string) = token.content {
                let slice = self.file_map.src_slice(string.content).unwrap();

                (string.ty, slice)
            } else {
                (StringType::SingleQuote, "error")
            }
        }

        pub fn node_path(&self, token: Token) -> (StringType, &str) {
            if let TokenContent::NodePath(string) = token.content {
                let slice = self.file_map.src_slice(string.content).unwrap();

                (string.ty, slice)
            } else {
                (StringType::SingleQuote, "error")
            }
        }

        pub fn comment(&self, token: Token) -> &str {
            if let TokenContent::Comment(comment) = token.content {
                let slice = self.file_map.src_slice(comment).unwrap();
                slice
            } else {
                "error"
            }
        }
    }

    #[test]
    fn empty_input() {
        let mut test = TokTest::new("");
        test.tokenize();
        let toks = test.toks;

        assert_eq!(toks.len(), 1);
        assert_eq!(toks[0].content, End);
    }

    #[test]
    fn newline_ignore() {
        use crate::token::Error::UnexpectedCharacter;
        {
            let mut test = TokTest::new("\\\n");
            test.tokenize();
            let toks = test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, TokenContent::End);
        }

        {
            let mut test = TokTest::new("\\\r\n");
            test.tokenize();
            let toks = test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, TokenContent::End);
        }

        {
            let mut test = TokTest::new("\\\r");
            test.tokenize();
            let toks = test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(
                toks[0].content,
                TokenContent::Error(UnexpectedCharacter {
                    expected: "newline after \'\\\'",
                    got: '\r',
                })
            );
        }

        {
            let mut test = TokTest::new("Hello\\\n    World");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 3);
            assert_eq!(test.identifier(toks[0]), "Hello");
            assert_eq!(test.identifier(toks[1]), "World");
            assert_eq!(toks[2].content, TokenContent::End);
        }

        {
            let mut test = TokTest::new("Hello\\\r\n    World");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 3);
            assert_eq!(test.identifier(toks[0]), "Hello");
            assert_eq!(test.identifier(toks[1]), "World");
            assert_eq!(toks[2].content, TokenContent::End);
        }

        {
            use crate::token::Error as E;
            let mut test = TokTest::new("\\    World");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(
                toks[0].content,
                TokenContent::Error(E::UnexpectedCharacter {
                    expected: "newline after '\\'",
                    got: ' ',
                })
            );
        }
    }

    #[test]
    fn newline() {
        {
            let mut test = TokTest::new("\n");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(toks[0].content, TokenContent::Newline);
            assert_eq!(toks[1].content, TokenContent::End);
        }

        {
            let mut test = TokTest::new("\r\n");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(toks[0].content, TokenContent::Newline);
            assert_eq!(toks[1].content, TokenContent::End);
        }

        {
            let mut test = TokTest::new("\r");
            test.tokenize();
            let toks = test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, TokenContent::End);
        }
    }

    #[test]
    fn keyword_operator_identifier() {
        use crate::token::Keyword as KeyW;
        use crate::token::Operator as Op;
        // identifier
        {
            let mut test = TokTest::new("hello");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.identifier(toks[0]), "hello");
            assert_eq!(toks[1].content, End);
        }

        // keyword identifier
        {
            let mut test = TokTest::new("extends Node");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 3);
            assert_eq!(toks[0].content, Keyword(KeyW::Extends));
            assert_eq!(test.identifier(toks[1]), "Node");
            assert_eq!(toks[2].content, End);
        }

        // keyword keyword identifier
        {
            let mut test = TokTest::new("export var x");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 4);
            assert_eq!(toks[0].content, Keyword(KeyW::Export));
            assert_eq!(toks[1].content, Keyword(KeyW::Var));
            assert_eq!(test.identifier(toks[2]), "x");
            assert_eq!(toks[3].content, End);
        }

        // operator
        {
            let mut test = TokTest::new("a and b or c");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 6);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Operator(Op::And));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, Operator(Op::Or));
            assert_eq!(test.identifier(toks[4]), "c");
            assert_eq!(toks[5].content, End);
        }
    }

    #[test]
    fn operators() {
        use crate::token::Operator as Op;

        {
            let mut test = TokTest::new("a && b || c");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 6);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Operator(Op::And));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, Operator(Op::Or));
            assert_eq!(test.identifier(toks[4]), "c");
            assert_eq!(toks[5].content, End);
        }

        {
            let mut test = TokTest::new("a * a + b * b == c * c");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 12);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Operator(Op::Mul));
            assert_eq!(test.identifier(toks[2]), "a");
            assert_eq!(toks[3].content, Operator(Op::Add));
            assert_eq!(test.identifier(toks[4]), "b");
            assert_eq!(toks[5].content, Operator(Op::Mul));
            assert_eq!(test.identifier(toks[6]), "b");
            assert_eq!(toks[7].content, Operator(Op::Equal));
            assert_eq!(test.identifier(toks[8]), "c");
            assert_eq!(toks[9].content, Operator(Op::Mul));
            assert_eq!(test.identifier(toks[10]), "c");
            assert_eq!(toks[11].content, End);
        }
    }

    #[test]
    fn punctuation() {
        use crate::token::Keyword as KeyW;
        use crate::token::Punctuation as Punc;
        use crate::token::Sign;

        {
            let mut test = TokTest::new("func test(a) -> void;");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 9);
            assert_eq!(toks[0].content, Keyword(KeyW::Func));
            assert_eq!(test.identifier(toks[1]), "test");
            assert_eq!(toks[2].content, Punctuation(Punc::ParenthesisOpen));
            assert_eq!(test.identifier(toks[3]), "a");
            assert_eq!(toks[4].content, Punctuation(Punc::ParenthesisClose));
            assert_eq!(toks[5].content, Punctuation(Punc::Arrow));
            assert_eq!(toks[6].content, Keyword(KeyW::Void));
            assert_eq!(toks[7].content, Punctuation(Punc::Semicolon));
            assert_eq!(toks[8].content, End);
        }

        {
            let mut test = TokTest::new("a.b");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 4);
            assert_eq!(test.identifier(toks[0]), "a");
            assert_eq!(toks[1].content, Punctuation(Punc::Period));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, End);
        }

        {
            let mut test = TokTest::new("1.2.b");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 4);
            assert_eq!(test.number(toks[0]), ("1", "2", Sign::Positive, ""));
            assert_eq!(toks[1].content, Punctuation(Punc::Period));
            assert_eq!(test.identifier(toks[2]), "b");
            assert_eq!(toks[3].content, End);
        }
    }

    #[test]
    fn string() {
        use crate::token::Error as ER;
        use crate::token::StringType as ST;

        // single quote

        {
            let mut test = TokTest::new("\'\'");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::SingleQuote, ""));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\'hello\'");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::SingleQuote, "hello"));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\'hello");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // double quote

        {
            let mut test = TokTest::new("\"\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, ""));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\"hello\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, "hello"));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\"hello");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // multi-line quote

        {
            let mut test = TokTest::new("\"\"\"\"\"\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::MultiLine, ""));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\"\"\"hello\"\"\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::MultiLine, "hello"));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\"\"\"hello\"\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // escape sequences

        {
            let mut test = TokTest::new("\"\\\"\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, "\\\""));
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("\"\\u048B\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.string(toks[0]), (ST::DoubleQuote, "\\u048B"));
            assert_eq!(toks[1].content, End);
        }
    }

    #[test]
    fn node_path() {
        use crate::token::Error as ER;
        use crate::token::StringType as ST;

        // single quote

        {
            let mut test = TokTest::new("@\'\'");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.node_path(toks[0]), (ST::SingleQuote, ""));
            assert_eq!(toks[1].content, End);
        }

        // double quote
        {
            let mut test = TokTest::new("@\"hello\"");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.node_path(toks[0]), (ST::DoubleQuote, "hello"));
            assert_eq!(toks[1].content, End);
        }

        // no string at all
        {
            let mut test = TokTest::new("@");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnexpectedEnd));
        }

        // unclosed string
        {
            let mut test = TokTest::new("@\'");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(toks[0].content, Error(ER::UnclosedStringLiteral));
        }

        // not a string
        {
            let mut test = TokTest::new("@a");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 1);
            assert_eq!(
                toks[0].content,
                Error(ER::UnexpectedCharacter {
                    got: 'a',
                    expected: "beginning quote of a string constant",
                })
            );
        }
    }

    #[test]
    fn comment() {
        {
            let mut test = TokTest::new("# Hi");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.comment(toks[0]), " Hi");
            assert_eq!(toks[1].content, End);
        }

        {
            let mut test = TokTest::new("# Hi\n");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 3);
            assert_eq!(test.comment(toks[0]), " Hi");
            assert_eq!(toks[1].content, Newline);
            assert_eq!(toks[2].content, End);
        }

        {
            let mut test = TokTest::new("hi # Hi");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 3);
            assert_eq!(test.identifier(toks[0]), "hi");
            assert_eq!(test.comment(toks[1]), " Hi");
            assert_eq!(toks[2].content, End);
        }

        {
            let mut test = TokTest::new("hi # Hi\n");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 4);
            assert_eq!(test.identifier(toks[0]), "hi");
            assert_eq!(test.comment(toks[1]), " Hi");
            assert_eq!(toks[2].content, Newline);
            assert_eq!(toks[3].content, End);
        }
    }

    #[test]
    fn numbers() {
        use crate::token::Sign;

        // hex
        {
            let mut test = TokTest::new("0xDEADBEEF");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.num_hex(toks[0]), "DEADBEEF");
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("0xDEADBEEF ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.num_hex(toks[0]), "DEADBEEF");
            assert_eq!(toks[1].content, End);
        }

        // "int"
        {
            let mut test = TokTest::new("42");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("42", "", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("42 ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("42", "", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }

        // "float" without exponent
        {
            let mut test = TokTest::new("13.37");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("13.37 ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }

        // "float" with positive exponent
        {
            let mut test = TokTest::new("13.37e12");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("13.37e12 ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("13.37e+12");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("13.37e+12 ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "float" with negative exponent
        {
            let mut test = TokTest::new("13.37e-12");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Negative, "12"));
            assert_eq!(toks[1].content, End);
        }
        {
            let mut test = TokTest::new("13.37e-12 ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "37", Sign::Negative, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "int" with positive exp
        {
            let mut test = TokTest::new("13e12");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        // "int" with negative exp
        {
            let mut test = TokTest::new("13e12 ");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "int" with dot with positive exp
        {
            let mut test = TokTest::new("13.e12");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Positive, "12"));
            assert_eq!(toks[1].content, End);
        }
        // "int" with dot with negative exp
        {
            let mut test = TokTest::new("13.e-12");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("13", "", Sign::Negative, "12"));
            assert_eq!(toks[1].content, End);
        }

        // "float" without integer part
        {
            let mut test = TokTest::new(".5");
            test.tokenize();
            let toks = &test.toks;

            assert_eq!(toks.len(), 2);
            assert_eq!(test.number(toks[0]), ("", "5", Sign::Positive, ""));
            assert_eq!(toks[1].content, End);
        }
    }

    #[test]
    fn inc_lex() {
        {
            let mut test_old = TokTest::new("Hello Old World");
            let mut test_new = TokTest::new("Hello Old Weird World");
            test_old.tokenize();
            let old_toks = &test_old.toks;
            test_new.tokenize_inc(Some(old_toks.to_vec()), Some((0, 11)));
            let new_toks = &test_new.toks;
            let new_report = &test_new.report;

            assert_eq!(new_toks.len(), 5);
            assert_eq!(new_report.is_inc, true);
            assert_eq!(new_report.old_tok_count, 2);
            assert_eq!(new_report.new_tok_count, 3);

            assert_eq!(test_new.identifier(new_toks[0]), "Hello");
            assert_eq!(test_new.identifier(new_toks[1]), "Old");
            assert_eq!(test_new.identifier(new_toks[2]), "Weird");
            assert_eq!(test_new.identifier(new_toks[3]), "World");
            assert_eq!(new_toks[4].content, TokenContent::End);
        }

        {
            let mut test_old = TokTest::new("Hello            \t\t\t\t\t            Old World");
            let mut test_new = TokTest::new("Hello Weird World");
            test_old.tokenize();
            let old_toks = &test_old.toks;
            test_new.tokenize_inc(Some(old_toks.to_vec()), Some((0, 8)));
            let new_toks = &test_new.toks;
            let new_report = &test_new.report;

            assert_eq!(new_toks.len(), 4);
            assert_eq!(new_report.is_inc, true);
            assert_eq!(new_report.old_tok_count, 1);
            assert_eq!(new_report.new_tok_count, 3);

            assert_eq!(test_new.identifier(new_toks[0]), "Hello");
            assert_eq!(test_new.identifier(new_toks[1]), "Weird");
            assert_eq!(test_new.identifier(new_toks[2]), "World");
            assert_eq!(new_toks[3].content, TokenContent::End);
        }
    }
}
